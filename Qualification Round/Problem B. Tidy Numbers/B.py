import sys

def IsListSorted_sorted(lst):
    return 

def check_sort(N):
    N = list(str(N))
    if sorted(N) == N:
        return True 
    else:
        return False  
    
input_file = open("B-large.in", "r")
output_file = open("B-large.txt", "w")
sys.stdout = output_file
lines = input_file.readlines()
T = lines[0].rstrip('\n')
T = int(T)
for case in xrange(T):
    N = lines[case+1].rstrip('\n')
    N = int(N)
    while not check_sort(N):
        N = N-1
    print 'Case #%d: %s' % (case+1, N)