def revenge_of_the_pancakes(N, P):
    stack = N.strip()
    ko = [stack[i:i+P] for i in xrange(0, len(stack), P)]
    print ko
    for i in ko:
        print i.count('-+')
    return P

input_file = open("A-small-attempt0.in", "r")
lines = input_file.readlines()
T = lines[0].rstrip('\n')
T = int(T)

for case in xrange(T):
    N = lines[case+1].rstrip('\n')
    P = int(filter(str.isdigit, N))
    N = N.split()[0] #this is input
    print 'Case #%d: %s' % (case+1, revenge_of_the_pancakes(N, P))